const express = require('express')
const app = express()
const port = 8090

app.get('/', (req, res) => {
  res.send('Hello World!! Testing dev branch blah blah blah!')
})

app.listen(port, () => {
  console.log('Example app listening at port %s', port)
})

// To run:
// cd project
// npm install
// npm start